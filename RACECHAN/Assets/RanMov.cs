﻿using UnityEngine;
using System.Collections;

public class RanMov : MonoBehaviour {
    private Vector3 moveDirection = Vector3.zero;
    public int randir;
    // Use this for initialization
    void Start ()
    {
        StartCoroutine(movfoe());
    }
	
	// Update is called once per frame
	void Update ()
    {
        
    }
    IEnumerator movfoe()
    {
        while (true)
        {
            randir = Random.Range(1, 4);
            switch (randir)
            {
                case 1:
                    transform.Translate(Vector3.forward * -3);
                    break;
                case 2:
                    transform.Translate(Vector3.forward * 3);
                    break;
                case 3:
                    transform.Rotate(Vector3.up, -3);
                    break;
                case 4:
                    transform.Rotate(Vector3.up, 3);
                    break;
                default:
                    transform.Translate(Vector3.forward * -3);
                    break;
            }
            yield return new WaitForSeconds(0.5f);
        }
    }
}
