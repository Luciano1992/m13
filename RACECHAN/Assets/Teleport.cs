﻿using UnityEngine;
using System.Collections;

public class Teleport : MonoBehaviour
{
    float x;
    float y;
    float z;
    Vector3 pos;
    GameObject player;
    public static bool teleport = false;
    // Use this for initialization
    void Start()
    {
        x = Random.Range(-50, 50);
        y = Random.Range(0, (Respawn.score * 2));
        z = Random.Range(-50, 50);
        pos = new Vector3(x, y, z);
        transform.position = pos;
    }

    // Update is called once per frame
    void Update()
    {
    }
    void Awake()
    {
        // Setting up the references.
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void OnTriggerEnter(Collider other)
    {
        x = Random.Range(-50, 50);
        y = 0;
        z = Random.Range(-50, 50);
        pos = new Vector3(x, y, z);
        transform.position = pos;
        Instantiate(this);
        teleport = true;


    }

}
