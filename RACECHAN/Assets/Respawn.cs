﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(Rigidbody))]

public class Respawn : MonoBehaviour {
    private Vector3 moveDirection = Vector3.zero;
    public static int score;
    public static int hp;
    int losthp;
    private Animator anim;
    private AnimatorStateInfo currentBaseState;
    bool gettel = Teleport.teleport;
    float x;
    float y;
    float z;
    Vector3 pos;
    // Use this for initialization
    void Start ()
    {
        score = 0;
        hp = 10;
        losthp = hp;
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (transform.position.y < -2)
        {
            transform.position = new Vector3(0, 0, 0);
            if (score > 0)
            {
                score--;
            }
        }
        if (hp <= 0)
        {
            transform.position = new Vector3(0, 0, 0);
            anim.SetBool("Jump", true);
            score = 0;
            hp = 10;
        }
        if(losthp>hp)
        {            
            losthp = hp;
            transform.Translate(Vector3.forward * -3);
            transform.Rotate(Vector3.up* 180);
        }
        if (Teleport.teleport)
        {
            x = Random.Range(-50, 50);
            y = 0;
            z = Random.Range(-50, 50);
            pos = new Vector3(x, y, z);
            transform.position = pos;
            Teleport.teleport = false;
        }
    }
}
