﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {
    float x;
    float y;
    float z;
    Vector3 pos;
    int getscore = Respawn.score;
    public GUIText scoreText;
    // Use this for initialization
    void Start () {
        x = Random.Range(-50, 50);
        y = Random.Range(0, (Respawn.score * 2));
        z = Random.Range(-50, 50);
        pos = new Vector3(x, y, z);
        transform.position = pos;
        scoreText.text = "Score: " + Respawn.score;
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = "Score: " + Respawn.score;
    }

    private void OnTriggerEnter(Collider other)
    {
        x = Random.Range(-50, 50);
        y = 0;
        z = Random.Range(-50, 50);
        pos = new Vector3(x, y, z);
        transform.position = pos;
        Respawn.score++;
        Instantiate(this);
    }

}
