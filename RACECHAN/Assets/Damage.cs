﻿using UnityEngine;
using System.Collections;

public class Damage : MonoBehaviour
{
    float x;
    float y;
    float z;
    Vector3 pos;
    int gethp = Respawn.hp;
    int getscore = Respawn.score;
    public GUIText hpText;
    // Use this for initialization
    void Start()
    {
        x = Random.Range(-50, 50);
        y = 0;
        z = Random.Range(-50, 50);
        pos = new Vector3(x, y, z);
        transform.position = pos;
        hpText.text = "HP: " + Respawn.hp;
    }

    // Update is called once per frame
    void Update()
    {
        hpText.text = "HP: " + Respawn.hp;
    }

    private void OnTriggerEnter(Collider other)
    {
        x = Random.Range(-50, 50);
        y = Random.Range(0, (Respawn.score*2));
        z = Random.Range(-50, 50);
        pos = new Vector3(x, y, z);
        transform.position = pos;
        Respawn.hp--;
        if (Respawn.hp < 0)
        {
            Respawn.hp = 0;
        }
        Instantiate(this);
    }

}

