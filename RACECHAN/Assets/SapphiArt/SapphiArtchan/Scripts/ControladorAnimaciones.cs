using UnityEngine;
using System.Collections;

public class ControladorAnimaciones : MonoBehaviour {

    internal string _Animation = null;
    internal string _EyesChangeType = null;
    internal string _EyebrowsChangeType = null;
    internal string _MouthChangeType = null;
    internal string _GeneralChangeType = null;
    internal float _FacialValue = 0.0f;
    internal bool _FacialValueBool = false;

    public void SetAnimation_Idle()
    {
        _Animation = "idle";
    }

    public void SetAnimation_Run()
    {
        _Animation = "running";
    }

    public void SetAnimation_Walk()
    {
        _Animation = "walk";
    }

    public void SetAnimation_Jump()
    {
        _Animation = "jump";
    }


    public void SetAnimation_WinPose()
    {
        _Animation = "winpose";
    }

    public void SetAnimation_KO()
    {
        _Animation = "ko_big";
    }

    public void SetAnimation_Damage()
    {
        _Animation = "damage";
    }

    public void SetAnimation_Hit01()
    {
        _Animation = "hit01";
    }

    public void SetAnimation_Hit02()
    {
        _Animation = "hit02";
    }

    public void SetAnimation_Hit03()
    {
        _Animation = "hit03";
    }
}
