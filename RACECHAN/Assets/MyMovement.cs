﻿using UnityEngine;

//
// アニメーション簡易プレビュースクリプト
// 2015/4/25 quad arrow
//

// Require these components when using this script
[RequireComponent(typeof(Animator))]

//altered
[RequireComponent(typeof(CharacterController))]
//end
public class MyMovement : MonoBehaviour
{

    private Animator anim;                      // Animatorへの参照
    private AnimatorStateInfo currentState;     // 現在のステート状態を保存する参照
    private AnimatorStateInfo previousState;	// ひとつ前のステート状態を保存する参照

    //altered
    public float speed = 3.0F;
    public float rotateSpeed = 3.0F;

    public float RotateSpeed = 30f;


    public float moveSpeed = 2f;
    public float turnSpeed = 70f;
    public float befjump;

    public bool running = false;
    public bool jumping = false;

    //public float speed = 6.0F;
    public float jumpSpeed = 5.0F;
    public float gravity = 10.0F;
    private Vector3 moveDirection = Vector3.zero;
    //end

    // Use this for initialization
    void Start()
    {
        // 各参照の初期化
        anim = GetComponent<Animator>();
        currentState = anim.GetCurrentAnimatorStateInfo(0);
        previousState = currentState;
        //moveDirection.y = 0;

        //altered
        //end

    }

public void runChar(){
     if (jumping == true && moveDirection.y >0)
            {
                moveDirection.y = jumpSpeed * (-4);
                anim.SetBool("RUN_00F", false);
                anim.SetBool("JUMP00", true);
                jumping = false;
                running = false;
                moveDirection.y = 0;
            }
            else
            {
                anim.SetBool("RUN_00F", true);
                running = true;
            }
}
public void backChar(){
     running = false;
            if (jumping == true && moveDirection.y > 0)
            {
                moveDirection.y = jumpSpeed * (-3);
                anim.SetBool("RUN_00F", false);
                anim.SetBool("WalkBack", true);
                jumping = false;
            }
            else
            {
                anim.SetBool("RUN_00F", false);
                //anim.SetBool("SAMK", true);
            }
}

    void FixedUpdate()
    {
        //altered by luciano

        if (Input.GetKey(KeyCode.UpArrow))
        {
           runChar();
        }
        if (running == true)
        {
            transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
           backChar();
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(Vector3.up, -turnSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(Vector3.up, turnSpeed * Time.deltaTime);
        }
        
        /*if (Input.GetKey(KeyCode.Space))
        {
            anim.SetBool("Run", false);
            running = false;
            anim.SetBool("Rising_P", true);
            transform.Translate(Vector3.up* moveSpeed * Time.deltaTime);
        }*/
        CharacterController controller = GetComponent<CharacterController>();
        if (controller.isGrounded)
        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;
            if (Input.GetKey(KeyCode.Space))
            {
                jumping = true;
                anim.SetBool("RUN_00F", false);
                running = false;
                moveDirection.y = jumpSpeed * 3;
                //moveDirection.y = jumpSpeed;
                transform.Translate(Vector3.forward * moveSpeed * 15 * Time.deltaTime);
                befjump = moveDirection.y;

            }


        }
        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);

        ;
    }

}
